/*
 * Ex01.c
 * Session 1: Hello C Program
 * Author: Mohamed Abdulaal
 */
// purpose: This program is part of C Course On AMIT Learning

/*
 * What's the expected output
 */
#include <stdio.h>
#include <stdlib.h>

int main()
{
    /* test Conditions */
    // 1- change the value of var1 to 1 and test
    // 2- change the logical and to logical or and test
    // 3- change the value of var1 to 2 and test
    char var1 = 2;
    char var2 = 2;
    int result = --var1 || var2++;
    printf("var1 = %d , var2 = %d \n",var1,var2);
    printf("result = %d \n",result);
    return 0;
}
