/*
 * Ex01.c
 * Session 1: Hello C Program
 * Author: Mohamed Abdulaal
 */
// purpose: This program is part of C Course On AMIT Learning

/* *******************************************************
 * Write C Code to convert 323 from Oct to Decimal
 * *****************output sample**************************
 * The output should be the decimal value of 323
 * which is 211
 * ********************************************************
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main()
{
    // This code to convert this Oct 323 to decimal
    int result ;
    result = 3*pow(8,0) + 2*pow(8,1) + 3*pow(8,2);
    printf("The decimal of 323 is = %d \n",result);
    return 0;
}
