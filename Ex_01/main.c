/*
 * Ex01.c
 * Session 1: Hello C Program
 * Author: Mohamed Abdulaal
 */
// purpose: This program is part of C Course On AMIT Learning

/* *******************************************************
 * Write C Code to convert 1101 0011 from Binary to Decimal
 * *****************output sample**************************
 * The output should be the decimal value of 1101 0011
 * which is 211
 * ********************************************************
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main()
{
    // This code to convert this binary 1101 0011 to decimal
    int result ;
    result = pow(2,0) + pow(2,1) + pow(2,4) + pow(2,6) + pow(2,7);
    int decimal = pow(2,0) + pow(2,1) + pow(2,4) + pow(2,6) + pow(2,7);
    printf("The decimal of 1101 0011 is = %d \n",result);
    return 0;
}
