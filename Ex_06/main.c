/*
 * Ex01.c
 * Session 1: Hello C Program
 * Author: Mohamed Abdulaal
 */
// purpose: This program is part of C Course On AMIT Learning

/* *******************************************************
 * What's the expected output
*/

#include <stdio.h>
#include <stdlib.h>

int main()
{
    printf("What's the expected output ?\n");
    int a = 20,b = 10,c = 15,d = 5,e;
    e = (a + b) * c / d;      // ( 30 * 15 ) / 5        90
    printf("Value of (a + b) * c / d is : %d\n",  e );

    e = ((a + b) * c) / d;    // (30 * 15 ) / 5         90
    printf("Value of ((a + b) * c) / d is  : %d\n" ,  e );

    e = (a + b) * (c / d);   // (30) * (15/5)           90
    printf("Value of (a + b) * (c / d) is  : %d\n",  e );

    e = a + (b * c) / d;     //  20 + (150/5)           50
    printf("Value of a + (b * c) / d is  : %d\n" ,  e );

    // 1 > 2 + 3 && 4 --> (1> (2+3) && 4)  --> ((1>5 ) && 4)  --> 0 && 4 = 0
    printf("1 > 2 + 3 && 4 = %d \n",(1 > 2 + 3 && 4));

    printf("%d \n",(10+9 * ((8+7)%6)+5*4%3*2+1));
    return 0;
}
