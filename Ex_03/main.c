/*
 * Ex01.c
 * Session 1: Hello C Program
 * Author: Mohamed Abdulaal
 */
// purpose: This program is part of C Course On AMIT Learning

/* *******************************************************
 * Write C Code to print the size of the following data types
 * (int , char , short , float, double)
 * *****************output sample**************************
 * The output should be the size of those types
 * ********************************************************
 */

#include <stdio.h>
#include <stdlib.h>

int main()
{
    printf("%d\n",sizeof(int));
    printf("%d\n",sizeof(char));
    printf("%d\n",sizeof(short));
    printf("%d\n",sizeof(float));
    printf("%d\n",sizeof(double));
    printf("%d %d %d\n",sizeof(3.14F),sizeof(3.14),sizeof(3.14l));
    return 0;
}
